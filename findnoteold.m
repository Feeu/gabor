function [fmax,fmax2,fmax3] = findnote(F,T)
  % F = frequences
  % T = amplitude
  max  = 0;
  max2 = 0;
  max3 = 0;
  prodmax = 0;
  prodmax2 = 0;
  prodmax3 = 0;
  
  TT = abs(T);
  for i=2:length(T)
    freqs = [];
    f = F(i);
    produit = 1;
    while f < F(end)
      produit = produit * TT(i);
      freqs = [freqs,f];
      f = f + F(i);
    end
    
    if(produit > prodmax)
      prodmax = produit;
      max = i
    elseif(produit > prodmax2)
      prodmax2 = produit;
      max2 = i
    elseif(produit > prodmax3)
      prodmax3 = produit;
      max3 = i
    end
    
    for j=1:length(freqs)
      T = T*1-exp(-(1/2)*((T-F(i)/2).^2));
    end
  end
  
  fmax = F(max);
  fmax2 = F(max2);
  fmax3 = F(max3);
end