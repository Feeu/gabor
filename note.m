close all
clc
clear

[y fs]=wavread('Parasol.wav');
figure(1);
t = [0:1/fs:(length(y)-1)*(1/fs)];
plot(t,y);

y = y(:,1);
N = 2048;
g = @(x,k,s) 1-exp(-(1/2).*((x-k)/((N/1024)*s)).^2);

yi = y(find(t>12.75 & t<13));
length(yi)
yi = yi(1:N);

Yi = fft(yi);
Yi = Yi(1:end/16);

f = [0:fs/(N-1):fs/16];

[fmax1,fmax2,fmax3]= findnote(f,Yi)

%{
figure(1);
plot(f,abs(Yi));

g = @(x,k,s) 1-exp(-(1/2).*((x-k)/((N/1024)*s)).^2);
t = [0:N];
figure(2);
plot(t,g(t,500,15));

figure(3);
plot(f,abs(Yi)'.*g(f,400,15));
