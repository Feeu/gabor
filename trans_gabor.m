function [Gab h t X Y]=trans_gabor(y,fs,M,sigma)




N=length(y);

T_fin=N/fs;

x=linspace(0,T_fin,M)';
t=linspace(0,T_fin,N)';




h=@(t,x)  exp(-(t*ones(1,length(x))-ones(N,1)*x').^2/sigma^2);



y2=y*ones(1,length(x)).*h(t,x);



Gab=fft(y2)/N;


NN=N/36;
Gab=Gab(1:floor(NN),:);

[X Y]=meshgrid(x,(linspace(1,floor(NN),floor(NN))/T_fin));






end
