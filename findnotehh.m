function [fmax1,fmax2,fmax3] = findnote(F,T)

  % F = frequences
  % T = amplitude
  max1  = 1;
  max2 = 1;
  max3 = 1;
  prodmax1 = 0;
  prodmax2 = 0;
  prodmax3 = 0;
  
  g = @(x,k,s) 1-exp(-(1/2).*((x-k)/((length(F)/1024)*s)).^2);
  TT = abs(T);
  amplitudemax = mean(TT)
  
  for i=2:length(T)/2
    if(TT(i)<amplitudemax)
      continue;
    end
    freqs = [];
    f = F(i);
    produit = 1;
#{
    for k=i:i:length(T)  
      produit = produit * TT(k);
      freqs = [freqs,f];
      k = k + i;
      f = f + F(i)-1;
    end
    #}
    produit = TT(i);
    if(produit > prodmax1)
      prodmax3 = max2;
      max3 = max2;
      prodmax2 = prodmax1;
      max2 = max1;
      
      prodmax1 = produit;
      max1 = i;
      TT = TT'.*g(F,F(max1),5);
    end

  end
  
  fmax1 = F(max1);
  fmax2 = F(max2);
  fmax3 = F(max3);
end