function []=cle_de_sol(nb_fig,X,couleur)


figure(nb_fig);
hold on

u(1)=X(1,1);
u(2)=X(1,end);
notes;

v=[do0;mi0;sol0;si0;re1;fa1]*[1 1];


plot(u,v(1,:),'g--')

plot(u,v(2,:),couleur)

plot(u,v(3,:),couleur)
plot(u,v(4,:),couleur)
plot(u,v(5,:),couleur)
plot(u,v(6,:),couleur)
