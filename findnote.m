function [fmax1,fmax2,fmax3] = findnote(F,T)

  % F = frequences
  % T = amplitude
  max1  = 1;
  max2 = 1;
  max3 = 1;
  prodmax1 = 0;
  prodmax2 = 0;
  prodmax3 = 0;
  g = @(x,k,s) 1-exp(-(1/2).*((x-k)/((length(F)/1024)*s)).^2);
 
  TT = abs(T);
  Mean = mean(TT);
  
  for i=2:length(TT)/2
    if TT(i)<Mean
      continue;
    end
    produit = 1;
    freqs = [];
    for k=i:i-1:length(TT)
      produit = produit * TT(k);
      freqs = [freqs,k];
    end
    if produit > prodmax1
      max3 = max2;
      max2 = max1;
      prodmax1 = produit;
      max1 = i;
      TT = TT.*g(F,F(i),500)';
    end
  end
  fmax1 = F(max1);
  fmax2 = F(max2);
  fmax3 = F(max3);
end